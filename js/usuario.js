$("document").ready(function () {

    // Botones back

    $(function () {
        $(".btnback")
            .eq(0).button()
            .end().eq(0).button({
                icon: "	ui-icon-arrowreturn-1-w"
            }).end().eq(1).button({
                icon: "	ui-icon-arrowreturn-1-w"
            })
    });

    $(".btnback").click(function (e) {
        e.preventDefault();
        window.location.replace("./index.html");

    });

    // Rellenar datos perfil

    $.ajax({
        type: "POST",
        url: "php/usuario.php",
        //data: datos,
        dataType: "json", //IMPORTANTE
        success: function (resultado) {

            $("div#avatar").append('<form enctype="multipart/form-data" id="avatar-form"><h2>Change avatar</h2><img width="200" src="img/avatar/' + resultado[0].avatar + '"><br /><label>Photo: <input type="file" name="foto" id="foto"></label><br/><input type="button" id="btnAvatar" value="Edit"></form>');

            $("div#datossesion").append('<form id="datos-sesion-form"><h2>Session data</h2><h5>Email</h5><input name="nuevoEmail" value="' + resultado[0].email + '" type="text"><br /><input type="button" id="btnSesion" value="Edit"></form><h5>Old password</h5><form id="password-form"><input name="viejaClave" type="password"><h5>New password</h5><input name="nuevaClave" type="password"><h5>Repeat new password</h5><input name="nuevaClaveRepetir" type="password"><br /><input type="button" id="btnPassword" value="Edit"></form>');

            $("div#datospersonales").append('<form id="datos-personales-form"><h2>Personal data</h2><h5>Name</h5><input name="nombre" type="text" value="' + resultado[0].nombre + '"><h5>Age</h5><input name="edad" type="text" value="' + resultado[0].edad + '"><br /><input type="button" id="btnPersonal" value="Edit"></form>')

            // visualizar las reservas hechas por este usuario
            $.ajax({
                type: "POST",
                url: "php/usuario_reservas.php",
                //data: datos,
                dataType: "json", //IMPORTANTE
                success: function (resultado) {

                    if (resultado.length != 0) {
                        $("div#viajesreservados").append('<h2>Reserved travells</h2>');
                        //Comentarios
                        $("div#viajesreservados").append('<form id="comentario-form">Viaje<select name="idReservaComentario" id="idReservaComentario"></select>Comment: <input name="comentario" type="text"><button id="comentario">Comment</button></form>');
                    }

                    for (var i = 0; i < resultado.length; i++) {

                        $("div#viajesreservados").append('<div id="' + resultado[i].id + '" class="reserva"><p>Travel name: ' + resultado[i].nombre + '</p><button class="cancelarViaje" id_Viaje="' + resultado[i].id_viaje + '" id="' + resultado[i].id + '">Cancel</button><button class="pdf" id="' + resultado[i].id_viaje + '">PDF</button></div>');

                        $("form#comentario-form select#idReservaComentario").append('<option value="' + resultado[i].id + '">' + resultado[i].nombre + '</option>')


                    }

                    $.ajax({
                        type: "POST",
                        url: "php/comentarios.php",
                        //data: datos,
                        dataType: "json", //IMPORTANTE
                        success: function (resultado) {

                            for (var i = 0; i < resultado.length; i++) {

                                $("div#" + resultado[i].id_reserva + "").append('<br/>Comentario ' + resultado[i].id + ' Mensaje: ' + resultado[i].comentario + '<button class="editarComentario" id="' + resultado[i].id + '">Edit</button><button class="borrarComentario" id="' + resultado[i].id + '">Delete</button><br/>');
                            }

                            $("div#viajesreservados").append('Texto para editar comentario: <form id="form-editar-comentario"><input name="editarComentarioInput" id="editarComentarioInput" type="text"></form>');

                            // Boton editar comentario
                            $(".editarComentario").on("click", function (e) {
                                e.preventDefault();

                                var idComentario = $(this).attr("id");
                                var comentarioEditado = $("#form-editar-comentario").serialize();
                                var datos = "idComentario=" + idComentario + "&" + comentarioEditado;

                                $.ajax({
                                    type: "POST",
                                    url: "php/usuario_comentario.php",
                                    data: datos,
                                    dataType: "json", //IMPORTANTE
                                    success: function (resultado) {
                                        window.open("./php/comentario_pdf.php?idComentario=" + idComentario);

                                        window.location.replace("./usuario.html");

                                    },
                                    error: function (xhr) {
                                        alert("Error al editar comentario");
                                    }

                                });

                            });
                            // Boton borrar comentario
                            $(".borrarComentario").on("click", function (e) {
                                e.preventDefault();

                                var idComentario = $(this).attr("id");
                                var datos = "idComentarioBorrar=" + idComentario;

                                $.ajax({
                                    type: "POST",
                                    url: "php/usuario_comentario.php",
                                    data: datos,
                                    success: function (resultado) {
                                        window.location.replace("./usuario.html");

                                    },
                                    error: function (xhr) {
                                        alert("Error al crear comentario");
                                    }

                                });

                            });
                        },
                        error: function (xhr) {
                            alert("No se pueden listar los comentarios");
                        }

                    });
                    // boton cancelar viaje
                    $(".cancelarViaje").on("click", function (e) {
                        e.preventDefault();

                        var idViaje = $(this).attr("id_viaje");
                        var idReserva = $(this).attr("id");

                        var datos = "idViaje=" + idViaje + "&idReserva=" + idReserva;

                        $.ajax({
                            type: "POST",
                            url: "php/usuario_operaciones.php",
                            data: datos,
                            success: function (resultado) {

                                window.location.replace("./usuario.html");

                            },
                            error: function (xhr) {
                                alert("Error al anular el viaje");
                            }

                        });

                    });

                    //boton PDF

                    $(".pdf").on("click", function (e) {
                        e.preventDefault();

                        var idViajePDF = $(this).attr("id");

                        var datos = "idViajepdf=" + idViajePDF;

                        window.open("./php/usuario_php.php?idViajepdf=" + idViajePDF);

                    });

                    //boton comentar

                    $("#comentario").on("click", function (e) {
                        e.preventDefault();

                        var datos = $("form#comentario-form").serialize();

                        $.ajax({
                            type: "POST",
                            url: "php/usuario_comentario.php",
                            data: datos,
                            success: function (resultado) {
                                window.location.replace("./usuario.html");

                            },
                            error: function (xhr) {
                                alert("Error al crear comentario");
                            }

                        });

                    });

                },
                error: function (xhr) {
                    alert("Error al visualizar reservas");
                }

            });

            // funciones validaciones
            tips = $("div#textoError");
            tipsBox = $("div#cuadradoError");

            function updateTips(t) {
                tips.text(t);
                tipsBox.show();
                setTimeout(function () {
                    tipsBox.hide();
                }, 5000);
            }

            function checkLength(o, n, min, max) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips("Length of " + n + " must be between " +
                        min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }

            function checkRegexp(o, regexp, n) {
                if (!(regexp.test(o.val()))) {
                    o.addClass("ui-state-error");
                    updateTips(n);
                    return false;
                } else {
                    return true;
                }
            }

            function samePassword(o, msg) {
                if (!(o.val() == nuevaPassword.val())) {
                    o.addClass("ui-state-error");
                    updateTips(msg);
                    return false;
                } else {
                    return true;
                }
            }

            $("input#btnSesion").click(function (e) {
                e.preventDefault();

                datos = $("form#datos-sesion-form").serialize();

                emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                    email = $("input[name='nuevoEmail']"),
                    allFields = $([]).add(email);

                var valid = true;

                allFields.removeClass("ui-state-error");

                valid = valid && checkLength(email, "email", 1, 80);

                valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");

                if (valid) {

                    $.ajax({
                        type: "POST",
                        url: "php/usuario_operaciones.php",
                        data: datos,
                        //dataType: "json", //IMPORTANTE
                        success: function (resultado) {
                            window.location.replace("./usuario.html");

                        },
                        error: function (xhr) {
                            alert("Error en PHP")
                        }

                    });
                }
                return valid;

            });

            $("input#btnPassword").click(function (e) {
                e.preventDefault();

                datos = $("form#password-form").serialize();

                $.ajax({
                    type: "POST",
                    url: "php/usuario_operaciones.php",
                    data: datos,
                    //dataType: "json", //IMPORTANTE
                    success: function (resultado) {
                        password = $("input[name='viejaClave']"),
                            nuevaPassword = $("input[name='nuevaClave']"),
                            repeatPassword = $("input[name='nuevaClaveRepetir']");

                        allFields = $([]).add(password).add(nuevaPassword).add(repeatPassword);

                        if (resultado == true) {

                            var valid = true;
                            allFields.removeClass("ui-state-error");

                            valid = valid && checkLength(nuevaPassword, "password", 1, 16);

                            valid = valid && checkRegexp(nuevaPassword, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");
                            valid = valid && samePassword(repeatPassword, "The password is not the same.");

                            if (valid) {
                                window.location.replace("./usuario.html");

                            }

                        } else if (resultado == false) {
                            password.addClass("ui-state-error");
                            updateTips("The old password is not correct.");

                        }



                    },
                    error: function (xhr) {
                        alert("Error en PHP")
                    }

                });

            });

            $("input#btnPersonal").click(function (e) {
                e.preventDefault();

                datos = $("form#datos-personales-form").serialize();

                nombre = $("input[name='nombre']"),
                    edad = $("input[name='edad']"),
                    allFields = $([]).add(nombre).add(edad);

                var valid = true;

                allFields.removeClass("ui-state-error");

                valid = valid && checkLength(nombre, "name", 1, 30);
                valid = valid && checkLength(edad, "age", 1, 3);

                valid = valid && checkRegexp(nombre, /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/, "Only letters and spaces");
                valid = valid && checkRegexp(edad, /^[0-9]*$/, "Only numbers");

                if (valid) {

                    $.ajax({
                        type: "POST",
                        url: "php/usuario_operaciones.php",
                        data: datos,
                        //dataType: "json", //IMPORTANTE
                        success: function (resultado) {
                            window.location.replace("./usuario.html");

                        },
                        error: function (xhr) {
                            alert("Error en PHP")
                        }

                    });
                }
                return valid;

            });

            $("input#btnAvatar").click(function (e) {
                e.preventDefault();

                if (document.getElementById("foto").files.length == 0) {
                    updateTips("Ningun archivo seleccionado.");
                } else {
                    //para recoger los campos FormData
                    var form = $('form#avatar-form')[0];
                    var data = new FormData(form);

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: "php/usuario_operaciones.php",
                        enctype: 'multipart/form-data', //necesario
                        processData: false, //necesario
                        contentType: false,//necesario
                        cache: false,
                        success: function (resultado) {
                            if (resultado == true) {
                                window.location.replace("./usuario.html");
                            }
                        },
                        error: function (xhr) {
                            alert(xhr.statusText + xhr.responseText);
                        },
                    });

                }

            });

        },
        error: function (xhr) {
            window.location.replace("./index.html");
        }

    });




});
