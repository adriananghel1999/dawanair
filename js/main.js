$("document").ready(function () {

    // Variable chapuza que sirve para comunicar un menu con un dialogo para editar los viajes siendo admin
    var GlobalparentId;

    // Variable para poder comunicarme con ajax al reservar un viaje
    var Globalidreserva;

    // Objetos array CONECTAR CON BBDD

    // Esto es para que el array empiece por 1 y no por 0
    var arrayViajes = [];
    var obj = new Object();
    arrayViajes.push(obj)
    delete arrayViajes[0];

    $.ajax({
        type: "POST",
        url: "php/index.php",
        dataType: "json", //IMPORTANTE
        success: function (datos) {

            for (var i = 0; i < datos.length; i++) {
                if (datos[i].reservado == 0){

                    var obj = new Object();
                    obj.id = datos[i].id;
                    obj.title = datos[i].nombre;
                    obj.price = datos[i].precio;
                    obj.img = datos[i].imagen;
                    obj.country = datos[i].nombre_pais;
                    obj.informacion = datos[i].informacion;
                    arrayViajes.push(obj);
                }

            }

            // Metiendo los paises dentro del select de manera dinamica

            $(arrayViajes).each(function (i) {

                $("select#country").append("<option value='" + this.country + "'>" + this.country + "</option>");

            });

            $("#country option[value='[object HTMLSelectElement]']").remove();

            // Quitar los paises que se repiten

            $("select#country option").val(function (idx, val) {
                $(this).siblings('[value="' + val + '"]').remove();
            });

            // Slider del flitro
            $(function () {
                $("#slider-range").slider({
                    range: true,
                    change: function (e, ui) {
                        search();
                        e.preventDefault();
                    },
                    min: 0,
                    max: 1500,
                    values: [0, 1500],
                    slide: function (event, ui) {
                        $("#amount").val("€" + ui.values[0] + " - €" + ui.values[1]);
                    }
                });
                $("#amount").val("€" + $("#slider-range").slider("values", 0) +
                    " - $" + $("#slider-range").slider("values", 1));
            });

        },
        error: function (xhr) {
            alert("Error con la BBDD, no se pueden dibujar los viajes");
        },
    });

    // Funcion para actualizar el array de viajes

    function updateArray() {

        for (var i = arrayViajes.length; i > 0; i--) {
            arrayViajes.pop();

        }

        var obj = new Object();
        arrayViajes.push(obj)
        delete arrayViajes[0];

        $.ajax({
            type: "POST",
            url: "php/index.php",
            dataType: "json", //IMPORTANTE
            success: function (datos) {

                for (var i = 0; i < datos.length; i++) {
                    
                    if (datos[i].reservado == 0){

                    var obj = new Object();
                    obj.id = datos[i].id;
                    obj.title = datos[i].nombre;
                    obj.price = datos[i].precio;
                    obj.img = datos[i].imagen;
                    obj.country = datos[i].nombre_pais;
                    obj.informacion = datos[i].informacion;
                    arrayViajes.push(obj);
                }

                }

                // Metiendo los paises dentro del select de manera dinamica

                $(arrayViajes).each(function (i) {

                    $("select#country").append("<option value='" + this.country + "'>" + this.country + "</option>");

                });

                $("#country option[value='[object HTMLSelectElement]']").remove();

                // Quitar los paises que se repiten

                $("select#country option").val(function (idx, val) {
                    $(this).siblings('[value="' + val + '"]').remove();
                });

            },
            error: function (xhr) {
                alert("Error con la BBDD");
            },
        });

    }

    // Dibujar array

    function drawArray(array) {
        $("div#principal").empty();

        $.ajax({
            type: "POST",
            url: "php/session.php",
            dataType: "json", //IMPORTANTE
            success: function (datos) {

                $(array).each(function (i) {

                    if (datos[0] == "admin") {

                        if (this.id == undefined) {

                        } else {
                            $("div#principal").append('<div id="' + this.id + '" class="reservas"><p class="titulo">' + this.title + '</p><button class="adminbutton" id="' + this.id + '"></button><img class="foto" src="../dawanairAjax/img/viajes/' + this.img + '"><p class="precio">' + this.price + '€</p><img id="' + this.id + '" class="reservar" src="../dawanairAjax/img/booking.png"></div>');
                        }

                    } else {

                        if (this.id == undefined) {

                        } else {
                            $("div#principal").append('<div id="' + this.id + '" class="reservas"><p class="titulo">' + this.title + '</p><img class="foto" src="../dawanairAjax/img/viajes/' + this.img + '"><p class="precio">' + this.price + '€</p><img id="' + this.id + '" class="reservar" src="../dawanairAjax/img/booking.png"></div>');
                        }
                    }
                });

                renovarEventosReservas();

            },
            error: function (xhr) {
                alert("Error con la BBDD");
            },
        });

    }

    // Botones menu principal

    $(function () {
        $(".btnwidget button")
            .eq(0).button()
            .end().eq(0).button({
                icon: "ui-icon-search"
            }).end().eq(1).button({
                icon: "ui-icon-key"
            }).end().eq(2).button({
                icon: "ui-icon-plus"
            }).end().eq(3).button({
                icon: "ui-icon-power"
            }).end().eq(4).button({
                icon: "ui-icon-document"
            }).end().eq(5).button({
                icon: "ui-icon-folder-open"
            })

    });

    // Boton avatar

    $(function () {
        $("#perfilAvatar").on("click", function () {
            window.location.replace("./usuario.html");
        });
    });

    // Boton manage imagenes

    $(function () {
        $("#btnimages").on("click", function () {
            window.location.replace("./images.html");
        });
    });

    // Funcion comprobar log

    function comprobarLog() {

        $(function () {

            $.ajax({
                type: "POST",
                url: "php/session.php",
                dataType: "json", //IMPORTANTE
                success: function (datos) {

                    if (datos[0] == "usuario") {
                        $("#btnnew").hide();
                        $("#btnlogin").hide();
                        $("#btnregister").hide();
                        $("button#btnlogout").show();
                        $("#perfilAvatar").show();
                        $("#btnimages").hide();
                        $("#perfilAvatar img").attr("src", "./img/avatar/" + datos[1]);

                    } else if (datos[0] == "invitado") {
                        $("#btnnew").hide();
                        $("#btnlogin").show();
                        $("#btnregister").show();
                        $("button#btnlogout").hide();
                        $("#perfilAvatar").hide();
                        $("#btnimages").hide();

                    }

                    else if (datos[0] == "admin") {
                        $("#btnnew").show();
                        $("#btnlogin").hide();
                        $("#btnregister").hide();
                        $("button#btnlogout").show();
                        $("#perfilAvatar").show();
                        $("#perfilAvatar img").attr("src", "./img/avatar/" + datos[1]);
                        $("#btnimages").show();
                    }

                    drawArray(arrayViajes);

                },
                error: function (xhr) {
                    alert("Error con la BBDD");
                },
            });


        });
    }


    // Botones back

    $(function () {
        $(".btnback")
            .eq(0).button()
            .end().eq(0).button({
                icon: "	ui-icon-arrowreturn-1-w"
            }).end().eq(1).button({
                icon: "	ui-icon-arrowreturn-1-w"
            })
    });

    // Eventos botones menu principal

    $("button#btnbuscar").on("click", function (e) {
        e.preventDefault();

        if ($("div#filter").css("display") == "none") {
            $("div#filter").fadeToggle();
            $("div#principal").animate({
                width: '65%'
            }, "slow");
            $("div.reservas").animate({
                marginLeft: '7%'
            }, "slow");


        } else {
            $("div#filter").fadeToggle();
            $("div#principal").animate({
                width: '90%'
            }, "slow");
            $("div.reservas").animate({
                margin: '3%'
            }, "slow");
        }

    });

    $("#btnlogout").on("click", function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "php/logout.php",
            success: function (datos) {
                location.reload();
            },
            error: function (xhr) {
                alert("Error con la BBDD");
            },
        });
    });

    $("#btnnew").on("click", function (e) {
        e.preventDefault();
        updateSelectmenuModal();
        $("#dialog-newtravel").dialog("open");

    });

    // Selectmenu de los modales con country

    function updateSelectmenuModal() {

        $('#countryModal')
            .find('option')
            .remove()
            .end()
            ;

        $('#countryModal2')
            .find('option')
            .remove()
            .end()
            ;

        $(arrayViajes).each(function (i) {

            $("#countryModal").append("<option value='" + this.country + "'>" + this.country + "</option>");
            $("#countryModal2").append("<option value='" + this.country + "'>" + this.country + "</option>");

        });

        $("#countryModal option[value='[object HTMLSelectElement]']").remove();
        $("#countryModal2 option[value='[object HTMLSelectElement]']").remove();

        // Quitar los paises que se repiten

        $("select#countryModal option").val(function (idx, val) {
            $(this).siblings('[value="' + val + '"]').remove();
        });

        $("select#countryModal2 option").val(function (idx, val) {
            $(this).siblings('[value="' + val + '"]').remove();
        });

    }

    $("#countryModal").selectmenu({
    });

    $("#countryModal2").selectmenu({
    });

    // Filtro

    $("select#country").selectmenu({
        change: function (e, ui) {
            search();
            e.preventDefault();
        }
    });

    // Funcion search

    function search(tamaño, margin) {
        var nuevoArray = [];

        // Coger valores filtro
        var nombre = $("input#nameInput").val().toLowerCase();
        var precioMin = $("#slider-range").slider("values", 0);
        var precioMax = $("#slider-range").slider("values", 1);
        var pais = $("select#country option:selected").text();

        $(arrayViajes).each(function (i) {

            if (this.id == undefined) { } else {

                if (pais == "None") {

                    if (this.title.toLowerCase().includes(nombre) && this.price >= precioMin && this.price <= precioMax) {
                        nuevoArray.push(arrayViajes[i]);
                    }
                } else {
                    if (this.title.toLowerCase().includes(nombre) && this.price >= precioMin && this.price <= precioMax && this.country == pais) {
                        nuevoArray.push(arrayViajes[i]);
                    }
                }

            }


        });

        drawArray(nuevoArray);

        if (!(tamaño == undefined) && !(tamaño == null) && !(margin == undefined) && !(margin == null)) {
            $("div#principal").animate({
                width: tamaño
            }, "slow");
            $("div.reservas").animate({
                marginLeft: margin
            }, "slow");
        } else {
            $("div#principal").animate({
                width: '65%'
            }, "slow");
            $("div.reservas").animate({
                marginLeft: '7%'
            }, "slow");
        }
    }

    $("input#nameInput").on("keyup", function (e) {
        search();

    });

    //Eventos botones back

    $("button#btnbackTop").on("click", function (e) {
        e.preventDefault();
        $("button#btnbackTop").fadeOut();
        $("button#btnbackBottom").fadeOut();
        search("90%", "3%");
        $("#btnbuscar").button('enable');
    });

    $("button#btnbackBottom").on("click", function (e) {
        e.preventDefault();
        $("button#btnbackTop").fadeOut();
        $("button#btnbackBottom").fadeOut();
        search("90%", "3%");
        $("#btnbuscar").button('enable');
    });

    function informacionReserva(idReserva) {
        $("div#principal").append('<div id="reservaIndividual"><div id="reservaTitulo">' + arrayViajes[idReserva].title + '</div><img src="../dawanairAjax/img/viajes/' + arrayViajes[idReserva].img + '" id="reservaImg"><div id="reservaInfo">' + arrayViajes[idReserva].informacion + '</div></div>');
        $("div#filter").fadeOut();
        $("div#principal").animate({
            width: '90%'
        }, "slow");
        $("div.reservas").animate({
            margin: '3%'
        }, "slow");
        $("#btnbuscar").button('disable');
    }

    // Funcion repetir eventos

    function renovarEventosReservas() {

        // Pulsar reservar
        $("img.reservar").on("click", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation()

            Globalidreserva = this.id;
            // Comprueba si es un usuario quien hace la reserva
            $.ajax({
                type: "POST",
                url: "php/session.php",
                dataType: "json", //IMPORTANTE
                success: function (datos) {

                    if (datos[0] == "usuario") {

                        var data = "idViajeReservar=" + Globalidreserva;

                        // Se hace la reserva
                        $.ajax({
                            type: "POST",
                            data: data,
                            url: "php/usuario_operaciones.php",
                            success: function (datos) {
                                window.location.href = './usuario.html';
                                window.scrollTo(0,document.body.scrollHeight);
                            },
                            error: function (xhr) {
                                alert("Error con la BBDD");
                            },
                        });

                    } else {
                        $("#dialog-message").dialog("open");
                    }

                },
                error: function (xhr) {
                    alert("Error con la BBDD");
                },
            });

        });

        //Click en reserva
        $("div.reservas").on("click", function (e) {

            e.preventDefault();
            $("div.reservas").fadeOut();
            var idViaje = $(this).attr("id");
            var indexViaje = arrayViajes.map(function (e) { return e.id; }).indexOf(idViaje);
            informacionReserva(indexViaje);
            $("button#btnbackTop").fadeIn();
            $("button#btnbackBottom").fadeIn();

        });

        //Filtro
        $("select#country").on("change", function (e) {
            search();
            e.preventDefault();

        });

        //Admin

        $(".adminbutton").button({
            icon: "ui-icon-gear"
        });

        $(".adminbutton").css({
            padding: "0px",
            position: "absolute",
            top: "0",
            right: "0px",
            display: "block",
            verticalAlign: "middle",
            textAlign: "center",
            margin: "0 auto"
        });

        $(".adminbutton").on("click", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation()

            thisId = $(this).attr("id");

            $("div#" + thisId).append('<div id="' + thisId + '" class="cuadradoAdminBox"><ul class="menuAdmin"><li><div><span class="ui-icon ui-icon-pencil"></span>Edit information</div></li><li><div><span class="ui-icon ui-icon-image"></span>Edit image</div></li><li><div><span class="ui-icon ui-icon-trash"></span>Delete</div></li><li><div><span class="ui-icon ui-icon-circle-close"></span>Close</div></li></ul></div>')
            $(".menuAdmin").menu();
            $(".menuAdmin").css({
                position: "absolute",
                right: "0",
                top: "0"
            });
            //editar viaje admin
            $(function () {
                $("div ul.menuAdmin li.ui-menu-item:nth-child(1)").on("click", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    GlobalparentId = $(this).closest("div.cuadradoAdminBox").attr("id");
                    var indexViaje = arrayViajes.map(function (e) { return e.id; }).indexOf(GlobalparentId);
                    $(this).parent().remove();
                    updateSelectmenuModal();
                    $("#dialog-edittravel").dialog("open");


                    $("#dialog-edittravel input#viajeEd").val(arrayViajes[indexViaje].title);
                    $("#dialog-edittravel input#priceEd").val(arrayViajes[indexViaje].price);
                    $("#dialog-edittravel input#countryEd").val(arrayViajes[indexViaje].country);
                    $("#dialog-edittravel textarea#informationEd").val(arrayViajes[indexViaje].informacion);

                });
            });
            //editar foto admin
            $(function () {
                $("div ul.menuAdmin li.ui-menu-item:nth-child(2)").on("click", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    GlobalparentId = $(this).closest("div.cuadradoAdminBox").attr("id");
                    var indexViaje = arrayViajes.map(function (e) { return e.id; }).indexOf(GlobalparentId);
                    $("#dialog-editimage").dialog("open");
                    $("#dialog-editimage-actual").attr("src", "../dawanairAjax/img/viajes/" + arrayViajes[indexViaje].img);
                    $(this).parent().remove();
                });
            });
            //borrar viaje admin
            $(function () {
                $("div ul.menuAdmin li.ui-menu-item:nth-child(3)").on("click", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    GlobalparentId = $(this).closest("div.cuadradoAdminBox").attr("id");
                    $("#dialog-confirm").dialog("open");
                    $(this).parent().remove();
                });
            });

            $(function () {
                $("div ul.menuAdmin li.ui-menu-item:nth-child(4)").on("click", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation()
                    $(this).parent().remove();
                });
            });


        });


    }

    // Dialogos

    // Alerta

    $(function () {
        $("#dialog-message").dialog({
            autoOpen: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    // Login YA TIENE AJAX NO TOCAR MAS

    $(function () {
        var dialog, form,

            emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
            email = $("#emaillogin"),
            password = $("#passwordlogin"),
            allFields = $([]).add(email).add(password),
            tips = $(".validateTips");

        // Funcion para actualizar mensaje dialogo
        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        // Funcion para comprobar longitud
        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        // Funcion para comprobar caracteres
        function checkRegexp(o, regexp, n) {
            if (!(regexp.test(o.val()))) {
                o.addClass("ui-state-error");
                updateTips(n);
                return false;
            } else {
                return true;
            }
        }

        function validUser() { // AJAX COMPROBAR SI USER ESTA EN BBDD

            var datos = $("#dialog-login-form").serialize();
            var res;

            $.ajax({
                type: "POST",
                url: "php/login.php",
                data: datos,
                dataType: "json", //IMPORTANTE
                success: function (resultado) {
                    //alert("Sesion iniciada " + resultado[0].email);
                    res = true;
                    loginUser(res);
                },
                error: function (xhr) {
                    updateTips("Incorrect email/password");
                    res = false;
                    loginUser(res);
                }

            });


        }

        // Funcion al pulsar login y todo esta bien AJAX LOGIN

        function loginUser(resultadoValidUser) {

            var valid = true;
            allFields.removeClass("ui-state-error");

            valid = valid && checkLength(email, "email", 1, 80);
            valid = valid && checkLength(password, "password", 1, 16);

            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

            valid = valid && resultadoValidUser;

            if (valid) {

                location.reload();

                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-login").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Log in": validUser,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            validUser();
        });

        $("#btnlogin").button().on("click", function () {
            dialog.dialog("open");
        });
    });

    // Register

    $(function () {
        var dialog, form,

            emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
            email = $("#emailregister"),
            password = $("#passwordregister"),
            repeatPassword = $("#Rpassword"),
            allFields = $([]).add(email).add(password).add(repeatPassword),
            tips = $(".validateTips");

        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp(o, regexp, n) {
            if (!(regexp.test(o.val()))) {
                o.addClass("ui-state-error");
                updateTips(n);
                return false;
            } else {
                return true;
            }
        }

        function samePassword(o, msg) {
            if (!(o.val() == password.val())) {
                o.addClass("ui-state-error");
                updateTips(msg);
                return false;
            } else {
                return true;
            }
        }

        function addUser() {
            var valid = true;
            allFields.removeClass("ui-state-error");

            valid = valid && checkLength(email, "email", 1, 80);
            valid = valid && checkLength(password, "password", 1, 16);
            valid = valid && checkLength(repeatPassword, "password", 1, 16);

            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");
            valid = valid && samePassword(repeatPassword, "The password is not the same.");

            if (valid) {

                var datos = $("#dialog-register-form").serialize();

                $.ajax({
                    type: "POST",
                    url: "php/register.php",
                    data: datos,
                    dataType: "json", //IMPORTANTE
                    success: function (resultado) {
                        alert("Registrado correctamente: " + resultado[0].email);
                    },
                    error: function (xhr) {
                        alert("Error con la base de datos (¿Email ya existente?).");
                    }

                });

                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-register").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Create an account": addUser,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            addUser();
        });

        $("#btnregister").button().on("click", function () {
            dialog.dialog("open");
        });
    });

    // Admin nuevo viaje

    $(function () {
        var dialog, form,

            name = $("input#viaje"),
            price = $("input#price"),
            information = $("textarea#information"),
            allFields = $([]).add(name).add(price).add(information),
            tips = $(".validateTips");

        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function addTravel() {
            var valid = true;
            allFields.removeClass("ui-state-error");

            valid = valid && checkLength(name, "name", 1, 30);
            valid = valid && checkLength(price, "price", 1, 5);
            valid = valid && checkLength(information, "information", 1, 200);

            if (valid) { // HACER ESTO CON BBDD

                var datos = $("#form-add-travel").serialize() + "&country=" + $("select#countryModal option:selected").text();

                $.ajax({
                    type: "POST",
                    data: datos,
                    url: "php/admin_operaciones.php",
                    success: function (resultados) {

                        window.location.href = './index.html';

                        /*updateArray();
                        renovarEventosReservas();
                        drawArray(arrayViajes);*/

                    },
                    error: function (xhr) {
                        alert("Error con la BBDD");
                    },
                });

                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-newtravel").dialog({
            autoOpen: false,
            height: 500,
            width: 600,
            modal: true,
            buttons: {
                "Create new travel": addTravel,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            addTravel();
        });
    });

    // Admin editar viaje

    $(function () {
        var dialog, form,

            name = $("input#viajeEd"),
            price = $("input#priceEd"),
            information = $("textarea#informationEd"),
            allFields = $([]).add(name).add(price).add(information),
            tips = $(".validateTips");

        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function editTravel() {
            var valid = true;
            allFields.removeClass("ui-state-error");

            valid = valid && checkLength(name, "name", 1, 30);
            valid = valid && checkLength(price, "price", 1, 5);
            valid = valid && checkLength(information, "information", 1, 200);

            if (valid) {

                var datos = $("#form-edit-travel").serialize() + "&country=" + $("select#countryModal2 option:selected").text() + "&id=" + GlobalparentId;

                $.ajax({
                    type: "POST",
                    data: datos,
                    url: "php/admin_operaciones.php",
                    success: function (resultados) {

                        window.location.href = './index.html';

                        /*updateArray();
                        renovarEventosReservas();
                        drawArray(arrayViajes);*/

                    },
                    error: function (xhr) {
                        alert("Error con la BBDD");
                    },
                });

                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-edittravel").dialog({
            autoOpen: false,
            height: 600,
            width: 500,
            modal: true,
            buttons: {
                "Edit travel": editTravel,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            editTravel();
        });


    });

    // Admin editar imagen

    $(function () {
        var dialog, form;

        function editTravelImage() {

            if (document.getElementById("newImage").files.length == 0) {
                alert("Ningun archivo seleccionado.");
            } else {
                //para recoger los campos FormData
                var form = $('form#form-edit-image')[0];
                var data = new FormData(form);
                data.append("id", GlobalparentId);

                $.ajax({
                    type: "POST",
                    data: data,
                    url: "php/admin_operaciones.php",
                    enctype: 'multipart/form-data', //necesario
                    processData: false, //necesario
                    contentType: false,//necesario
                    cache: false,
                    success: function (resultado) {
                        if (resultado == true) {
                            window.location.replace("./index.html");
                        } else {
                            alert(resultado);
                        }

                        dialog.dialog("close");
                    },
                    error: function (xhr) {
                        alert("Error al editar imagen de viaje");
                    },
                });

            }
        }

        dialog = $("#dialog-editimage").dialog({
            autoOpen: false,
            height: 600,
            width: 500,
            modal: true,
            buttons: {
                "Edit travel": editTravelImage,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            editTravelImage();
        });


    });

    // Dialogo confirmacion

    $(function () {
        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Delete": function () {

                    var datos = "idBorrar=" + GlobalparentId;

                    $.ajax({
                        type: "POST",
                        data: datos,
                        url: "php/admin_operaciones.php",
                        success: function (resultados) {

                            window.location.href = './index.html';

                        },
                        error: function (xhr) {
                            alert("Error con la BBDD");
                        },
                    });
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    comprobarLog();

});
