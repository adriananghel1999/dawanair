$("document").ready(function () {

    // Botones back

    $(function () {
        $(".btnback")
            .eq(0).button()
            .end().eq(0).button({
                icon: "	ui-icon-arrowreturn-1-w"
            }).end().eq(1).button({
                icon: "	ui-icon-arrowreturn-1-w"
            })
    });

    $(".btnback").click(function (e) {
        e.preventDefault();
        window.location.replace("./index.html");

    });

    // Rellenar con imagenes

    $.ajax({
        type: "POST",
        url: "php/admin_imagenes.php",
        //data: datos,
        dataType: "html", //IMPORTANTE
        success: function (resultado) {

            $("div#imagenes").append(resultado);

            // BOton borrar imagen
            $(function () {
                $(".borrarImagen").on("click", function () {
                    var nombreImagen = $(this).attr("id");
                    var datos = "imagenBorrar="+nombreImagen;
                    $.ajax({
                        type: "POST",
                        data: datos,
                        url: "php/admin_operaciones.php",
                        success: function (resultados) {
                            window.location.href = './images.html';
        
                        },
                        error: function (xhr) {
                            alert("Error con la BBDD");
                        },
                    });
                });
            });

        },
        error: function (xhr) {
            alert("Error en admin imagenes");
        }

    });

    

});
