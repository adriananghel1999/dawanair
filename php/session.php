<?php
include("conexion.php");

session_start();

// Return de si hay conexion o no 

if(isset($_SESSION["admin"]) && isset($_SESSION["email"])){

    $datos = array("admin", $_SESSION['avatar']);
    echo json_encode($datos);

} else if (isset($_SESSION["email"])) {

    $datos = array("usuario", $_SESSION['avatar']);
    echo json_encode($datos);

} else {

    $datos = array("invitado");
    echo json_encode($datos);

}

mysqli_close($conexion);
?>