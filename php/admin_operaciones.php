<?php
include("conexion.php");
session_start();

if ( isset($_SESSION["admin"])){
    // añadir viaje
   if ( isset($_POST["viaje"]) && isset($_POST["price"]) && isset($_POST["information"]) ){

    $viaje = $_POST["viaje"];
    $price = $_POST["price"];
    $country = $_POST["country"];
    $information = $_POST["information"];

    $sql=$conexion->query("SELECT * FROM `paises` WHERE `nombre` LIKE '$country'");
    $fila = mysqli_fetch_array($sql);
    $id_pais = $fila["id"];

     $nombreImagen = "viaje.jpg"; 

    $conexion->query("INSERT INTO `viajes` (`id`, `precio`, `nombre`, `id_pais`, `imagen`, `informacion`) VALUES (NULL, '$price', '$viaje', '$id_pais', '$nombreImagen', '$information');");

    } 
    // editar viaje
    if ( isset($_POST["viajeEd"]) && isset($_POST["priceEd"]) && isset($_POST["informationEd"]) ){

    $viaje = $_POST["viajeEd"];
    $price = $_POST["priceEd"];
    $country = $_POST["country"];
    $information = $_POST["informationEd"];
    $id = $_POST["id"];

    $sql=$conexion->query("SELECT * FROM `paises` WHERE `nombre` LIKE '$country'");
    $fila = mysqli_fetch_array($sql);
    $id_pais = $fila["id"];

    $conexion->query("UPDATE `viajes` SET `precio` = '$price', `nombre` = '$viaje', `id_pais` = '$id_pais', `informacion` = '$information' WHERE `viajes`.`id` = '$id';");

    }
    //cambiar foto viaje
    if ($_FILES["newImage"]["size"] != 0){

        $nombreImagen = $_FILES["newImage"]["name"];
        $id = $_POST["id"];
    
      $error = "";
      $maxsize    = 4097152;
      $acceptable = array(
          'image/jpeg',
          'image/jpg',
          'image/png'
      );
  
      if(($_FILES['newImage']['size'] >= $maxsize) || ($_FILES["newImage"]["size"] == 0)) {
          $error= 'Demasiado grande.';
      }
  
      if((!in_array($_FILES['newImage']['type'], $acceptable)) && (!empty($_FILES["newImage"]["type"]))) {
          $error = 'Solo JPG, JPEG y PNG';
      }
  
      if($error == "") {
          move_uploaded_file($_FILES['newImage']['tmp_name'], "./../img/viajes/" . $_FILES["newImage"]["name"]);
          chmod("./../img/viajes", 0777);
          
          $sql=$conexion->query("UPDATE `viajes` SET `imagen` = '$nombreImagen' WHERE `viajes`.`id` = '$id';");
          
          echo true;
        } else {
              echo $error;
  
      }
    }
// BOrrar viaje
    if (isset($_POST['idBorrar'])){
        $id = $_POST['idBorrar'];

        $sql=$conexion->query("DELETE FROM `viajes` WHERE `viajes`.`id` = '$id'");
        
    }
// Borrar imagen
    if (isset($_POST['imagenBorrar'])){
        $imagenBorrar = $_POST["imagenBorrar"];
        unlink("./../img/viajes/" . $imagenBorrar);
    }


} else {
    header("location:./../index.html");
}




mysqli_close($conexion);

?>