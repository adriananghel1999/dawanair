-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2020 a las 20:27:56
-- Versión del servidor: 10.0.38-MariaDB-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.33-0ubuntu0.16.04.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dawanair`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cometarios`
--

CREATE TABLE `cometarios` (
  `id` int(11) NOT NULL,
  `id_reserva` int(11) NOT NULL,
  `comentario` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cometarios`
--

INSERT INTO `cometarios` (`id`, `id_reserva`, `comentario`) VALUES
(3, 6, 'TOkyo town'),
(4, 5, 'Berlin coment');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`) VALUES
(1, 'USA'),
(2, 'Spain'),
(3, 'Germany'),
(4, 'Romania'),
(5, 'UK'),
(6, 'Rusia'),
(7, 'China'),
(8, 'Japan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `id` int(11) NOT NULL,
  `id_viaje` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`id`, `id_viaje`, `id_usuario`) VALUES
(8, 2, 3),
(5, 4, 1),
(6, 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `email` varchar(70) NOT NULL,
  `clave` varchar(70) NOT NULL,
  `admin` varchar(5) NOT NULL DEFAULT 'false',
  `avatar` varchar(50) NOT NULL DEFAULT 'default.png',
  `nombre` varchar(50) NOT NULL DEFAULT 'Introduzca su nombre completo',
  `edad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `clave`, `admin`, `avatar`, `nombre`, `edad`) VALUES
(1, 'usuario@mail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'false', 'QrzR94w.jpg', 'Usuario Fernandez', 25),
(2, 'admin@mail.com', 'd93591bdf7860e1e4ee2fca799911215', 'true', 'avataradmin.png', 'Admin Martinez', 32),
(3, 'adriananghel1999@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'false', '1.png', 'Adrian', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajes`
--

CREATE TABLE `viajes` (
  `id` int(11) NOT NULL,
  `precio` int(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `informacion` text NOT NULL,
  `reservado` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `viajes`
--

INSERT INTO `viajes` (`id`, `precio`, `nombre`, `id_pais`, `imagen`, `informacion`, `reservado`) VALUES
(1, 1500, 'Hawaii', 1, 'hawaii.jpeg', 'Hawái es un estado de EE. UU. situado en un archipiélago volcánico del Pacífico central. Sus islas son famosas por sus paisajes escarpados con acantilados, cascadas, selvas tropicales y playas con arena de color dorado, rojo, negro e incluso verde. De las 6 islas principales de Hawái, Oahu es donde está situada Honolulu, la capital del estado y su ciudad más grande. En esta isla se encuentra la playa de Waikiki, con forma de luna creciente, y monumentos conmemorativos del ataque a Pearl Harbor, que se produjo durante la Segunda Guerra Mundial.', 0),
(2, 1200, 'Los Angeles', 1, 'angeles.jpg', 'Los Ángeles, oficialmente Ciudad de Los Ángeles y de manera abreviada L. A., es la ciudad más poblada del estado estadounidense de California y la segunda ciudad más poblada de Estados Unidos. Tiene, según el censo de 2010, una población de 3 792 621 habitantes.​', 1),
(3, 850, 'Barcelona', 2, 'barcelona.jpg', 'Barcelona es una ciudad española, capital de la comunidad autónoma de Cataluña, de la comarca del Barcelonés y de la provincia homónima. Con una población de 1 620 343 habitantes en 2018, ​ es la segunda ciudad más poblada de España después de Madrid, y la undécima de la Unión Europea.', 0),
(4, 1000, 'Berlin', 3, 'berlin.jpg', 'Berlín es la capital de Alemania y uno de los dieciséis estados federados alemanes. Se localiza al noreste de Alemania. Por la ciudad fluyen los ríos Esprea, Havel, Panke, Dahme y Wuhle.', 1),
(5, 500, 'Bucuresti', 4, 'bucuresti.jpeg', 'Bucarest es la capital y ciudad más poblada de Rumania, así como su principal centro industrial, comercial y cultural. Está situada en el sureste del país, a orillas del río Dambovita.', 0),
(6, 750, 'Londres', 5, 'londres.jpg', 'Londres es la capital y mayor ciudad de Inglaterra y del Reino Unido.​​ Situada a orillas del río Támesis, Londres es un importante asentamiento humano desde que fue fundada por los romanos con el nombre de Londinium hace casi dos milenios.', 0),
(7, 750, 'Madrid', 2, 'madrid.jpg', 'Madrid es un municipio y ciudad de España. La localidad, con categoría histórica de villa, ​ es la capital del Estado​ y de la Comunidad de Madrid. Dentro del término municipal de Madrid, el más poblado de España, viven 3 223 334 personas empadronadas, según el INE de 2018.', 0),
(8, 850, 'Moscow', 6, 'moscu.jpeg', 'Moscú es la capital y la entidad federal más poblada de Rusia. La ciudad es un importante centro político, económico, cultural y científico de Rusia y del continente. Moscú es la megaciudad más septentrional de la Tierra, la segunda ciudad de Europa en población después de Estambul, ​​​ y la sexta del mundo.', 0),
(9, 1400, 'Pekin', 7, 'pekin.jpg', 'Pekín es uno de los cuatro municipios que, junto con las veintidós provincias, cinco regiones autónomas y dos regiones administrativas especiales, conforman la República Popular China.', 0),
(10, 1350, 'Tokyo', 8, 'tokyo.jpg', 'Tokio es la capital de facto​ de Japón, localizada en el centro-este de la isla de Honshu, concretamente en la región de Kanto. En conjunto forma una de las 47 prefecturas de Japón, aunque su denominación oficial es metrópolis o capital.', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cometarios`
--
ALTER TABLE `cometarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_reserva` (`id_reserva`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_ajenas` (`id_viaje`,`id_usuario`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `EmailUnico` (`email`);

--
-- Indices de la tabla `viajes`
--
ALTER TABLE `viajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pais` (`id_pais`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cometarios`
--
ALTER TABLE `cometarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `viajes`
--
ALTER TABLE `viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cometarios`
--
ALTER TABLE `cometarios`
  ADD CONSTRAINT `cometarios_ibfk_1` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id`);

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `reservas_ibfk_1` FOREIGN KEY (`id_viaje`) REFERENCES `viajes` (`id`),
  ADD CONSTRAINT `reservas_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `viajes`
--
ALTER TABLE `viajes`
  ADD CONSTRAINT `viajes_ibfk_1` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
